﻿using DataClass.Models.NewModels;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using HotelSystem.Models.Reference;
using Microsoft.EntityFrameworkCore;

namespace SysHotel.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// Auth tables
        public DbSet<User_position> User_positions { get; set; }
        public DbSet<User_role> User_roles { get; set; }
        public DbSet<User> Users { get; set; }
        /// <summary>
        /// reference tables
        /// </summary>
        public DbSet<Gender> Genders { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus_type> OrderStatus_type { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Payment_type> Payment_types { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomCapasity> RoomCapasitys { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<PaymentClient> PaymentClients { get; set; }
        public DbSet<Expenses> Expenseses { get; set; }
        public DbSet<Expenses_Type> Expenses_Types { get; set; }

       
    }
}