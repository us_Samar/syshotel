﻿using DataClass.Entity.Implement;
using DataClass.Entity.Interface;
using DataClass.Models.NewModels;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using HotelSystem.Models.Reference;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Service
{
    public class OrderService : Repository<Order>, IOrderService
    {
        private readonly ApplicationDbContext _db;
        private readonly IClientService _clientService;

        public OrderService(ApplicationDbContext repositoryContext,
        ApplicationDbContext db, IClientService clientService
        ) : base(repositoryContext)
        {
            _db = db;
            _clientService = clientService;
        }
        public IList<OrdersViewModel> GetOrdersViewList()
        {
            List<OrdersViewModel> ordersViewModels = new List<OrdersViewModel>();

            var orederList = _db.Orders.Where(m => m.OrderStatus_typeId == 1).ToList();
            if (orederList != null)
            {
                foreach (var item in orederList)
                {
                    var PayClient = _db.PaymentClients.Where(m => m.ID == item.PaymentClientId).FirstOrDefault();

                    OrdersViewModel viewModel = new OrdersViewModel();
                    viewModel.Id = item.Id;
                    viewModel.AllName = _clientService.FIO(PayClient.ClientId);
                    viewModel.Payment = item.DayPayment.ToString();
                    viewModel.AllPayment = PaymentDay(item.Id);
                    viewModel.PaymentType = AboutPay(PayClient.PaymentId).PaymentTypeName;
                    viewModel.Comment = item.Comment;
                    viewModel.RoomName = _db.Rooms.First(m=>m.Id==item.RoomId).Name;
                    viewModel.Gender = _db.Clients.Where(m=>m.Id==PayClient.ClientId).First().GenderId;
                    viewModel.Status = item.OrderStatus_typeId;
                    viewModel.RoomId = item.RoomId;
                    viewModel.Brom = item.Brom;
                    //viewModel.PaymentStatus=DateTime.Now.Day-item.DayOfDelivery.Day

                    ordersViewModels.Add(viewModel);
                }
            }
            return ordersViewModels;
        }

        public void AddOrders(AddNewOrderViewModel order)
        {
            using (var transaction = _db.Database.BeginTransaction())
            {
                Payment payment = new Payment();
                payment.PaymentValue = order.Payment;
                payment.Payment_typeId = order.Payment_type;
                payment.PaymentTime = DateTime.Now;
                _db.Payments.Add(payment);
                _db.SaveChanges();

                PaymentClient paymentClient = new PaymentClient();
                paymentClient.PaymentId= payment.Id;
                paymentClient.ClientId = order.ClientID;
                _db.PaymentClients.Add(paymentClient);
                _db.SaveChanges();

                Order ord = new Order();
                ord.PaymentClientId = paymentClient.ID;
                ord.DayPayment = order.PaymentForDay;
                ord.Comment = order.Comment==null?"":order.Comment;
                ord.RoomId = order.RoomName;
                ord.TimeOfDelivery = DateTime.Now;
                ord.CheckOutTime = DateTime.Now;
                ord.OrderStatus_typeId = 1;
                ord.Brom = order.AllBusy;
                ord.UserId = order.UserID;
                ord.DayOfDelivery = DateTime.Now.Hour > 5 ? DateTime.Now : ord.TimeOfDelivery.AddDays(-1);
                _db.Orders.Add(ord);
                _db.SaveChanges();

                transaction.Commit();

            }

        }
        public void UpdateOrders(EditOrderViewModel order)
        {
            using (var transaction = _db.Database.BeginTransaction())
            {
                var FindOrder=_db.Orders.Find(order.Id);
                
                FindOrder.DayPayment = order.PaymentForDay;
                FindOrder.Comment = order.Comment == null ? "" : order.Comment;
                FindOrder.RoomId = order.RoomID;
                FindOrder.Brom = order.AllBusy;
                _db.Orders.Update(FindOrder);
                _db.SaveChanges();

                transaction.Commit();
            }

        }
        public PaymentViewModel AboutPay(int PaymentId)
        {
            PaymentViewModel viewModel = new PaymentViewModel();
            var Pay = _db.Payments.Where(m => m.Id == PaymentId).FirstOrDefault();
            if (Pay != null)
            {
                viewModel.Id = PaymentId;
                viewModel.Payment = Pay.PaymentValue.ToString();
                viewModel.PaymentTypeName = _db.Payment_types.Where(m => m.Id == Pay.Payment_typeId).
                    FirstOrDefault().Name;
            }
            return viewModel;
        }
        private int PaymentDay(int OrderID)
        {
            int result = 0,checkDay=0;
            if (DateTime.Now.Hour>=12)
            {
                checkDay = 1;
            }
            var order = _db.Orders.Find(OrderID);
            if (order != null)
            {
                int hour = order.DayOfDelivery.Hour;
                TimeSpan value= DateTime.Now.Subtract(order.DayOfDelivery); ;
                if (DateTime.Now.Hour<hour)
                {
                    hour = hour-DateTime.Now.Hour + 1;
                    value = DateTime.Now.AddHours(hour).Subtract(order.DayOfDelivery);
                }
                
                int day = value.Days;
                var paymentOrder = _db.PaymentClients.Find(order.PaymentClientId);
                var paymentOrder1 = _db.PaymentClients.Where(m => m.ClientId == paymentOrder.ClientId).ToList();
                foreach (var item in paymentOrder1)
                {
                    var payment = _db.Payments.Find(item.PaymentId);
                    if (payment != null && payment.PaymentTime > order.TimeOfDelivery.AddHours(-1))
                    {
                        result = result + payment.PaymentValue;
                    }
                }
                result = result - ((day+ checkDay) * order.DayPayment);
            }
            return result;
        }
    }
}
