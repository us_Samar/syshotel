﻿using DataClass.Entity.Implement;
using DataClass.Entity.Interface;
using DataClass.Models.NewModels;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Service
{
    public class PaymentService : Repository<Payment>, IPaymentService
    {
        private readonly ApplicationDbContext _db;
        public PaymentService(ApplicationDbContext repositoryContext,
            ApplicationDbContext db)
            : base(repositoryContext)
        {
            _db = db;
        }

        public void AddPay(AddPayViewModel model)
        {
            using (var transaction = _db.Database.BeginTransaction())
            {
                Payment payment = new Payment();
                payment.PaymentValue = model.Payment;
                payment.Payment_typeId = model.Payment_type;
                payment.PaymentTime = DateTime.Now;
                payment.UserId = model.UsertId;
                _db.Payments.Add(payment);
                _db.SaveChanges();

                PaymentClient payClient = new PaymentClient();
                payClient.PaymentId = payment.Id;
                payClient.ClientId = model.ClientId;
                _db.PaymentClients.Add(payClient);
                _db.SaveChanges();

                var findOrder = _db.Orders.Find(model.OrderId);
                findOrder.PaymentClientId = payClient.ID;
                _db.Orders.Update(findOrder);
                _db.SaveChanges();
                transaction.Commit();

            }
        }

        public DayReportViewModel DayReport()
        {
            
            int a = 0;
            DayReportViewModel model = new DayReportViewModel();
            List<ListClass> list = new List<ListClass>();
            List<ListClass> list1 = new List<ListClass>();
            var PayTypeList = _db.Payment_types.ToList();
            foreach (var item in PayTypeList)
            {
                a++;
                ListClass modelList = new ListClass();
                var Payment = _db.Payments.Where(m =>
                m.PaymentTime.Day == DateTime.Now.Day &&
                m.PaymentTime.Month == DateTime.Now.Month &&
                m.PaymentTime.Year == DateTime.Now.Year &&
                m.Payment_typeId == item.Id).Sum(m => m.PaymentValue);
                modelList.Id = a;
                modelList.Name = Payment.ToString();
                modelList.Type = item.Name;
                list.Add(modelList);
                ListClass modelList1 = new ListClass();
                var Expenses = _db.Expenseses.Where(m =>
                m.Expens_date.Day == DateTime.Now.Day &&
                m.Expens_date.Month == DateTime.Now.Month &&
                m.Expens_date.Year == DateTime.Now.Year &&
                m.Payment_typeId == item.Id).Sum(m => m.Expense);
                modelList1.Id = a;
                modelList1.Name = Expenses.ToString();
                modelList1.Type = item.Name;
                list1.Add(modelList1);
            }
            model.PayList = list;
            model.ExpenList = list1;
            return model;
        }

    }
}
