﻿using DataClass.Entity.Implement;
using DataClass.Entity.Interface;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Service
{
    public class DistrictService : Repository<District>, IDistrictService
    {
        public DistrictService(ApplicationDbContext repositoryContext)
            : base(repositoryContext)
        {

        }


    }
}
