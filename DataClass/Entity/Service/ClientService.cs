﻿using DataClass.Entity.Implement;
using DataClass.Entity.Interface;
using DataClass.Models;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Service
{
    public class ClientService : Repository<Client>, IClientService
    {
        private readonly ApplicationDbContext _db;
        public ClientService(ApplicationDbContext repositoryContext,
            ApplicationDbContext db)
            : base(repositoryContext)
        {
            _db = db;
        }
        public int SearchClient(ClientFilterModel model)
        {
            var foundClient=_db.Clients.Where(m=>m.Passport_series==model.PassSeria&&m.Passport_number==model.PassNumber).FirstOrDefault();
            if (foundClient==null)
            {
                return 0;
            }
            return foundClient.Id;
        }
        public string FIO(int id)
        {
            var find = _db.Clients.FirstOrDefault(m => m.Id == id);
            string result = "";
            if (find != null)
            {
                result = find.FirstName + " " + find.LastName + " " + find.MiddleName;
            }
            return result;
        }
        public string GenderName(int ClientID)
        {
            var client=_db.Clients.Where(m => m.Id == ClientID).First();
            return _db.Genders.Where(m => m.Id == client.GenderId).First().Name;
        }

        public IEnumerable<ClientViewModel> GetClient()
        {
            List<ClientViewModel> result = new List<ClientViewModel>();
            var clientList = _db.Clients.ToList();
            foreach (var client in clientList)
            {

                ClientViewModel model = new ClientViewModel();
                model.Id = client.Id;
                model.FirstName = client.FirstName;
                model.LastName= client.LastName;
                model.MiddleName= client.MiddleName;
                model.Date_of_Birth = (DateTime)client.Date_of_birth;
                model.Passport_Series = client.Passport_series;
                model.Passport_Number = client.Passport_number;
                model.GenderName=_db.Genders.Find(client.GenderId).Name;                
                result.Add(model);
            }
            return result;
        }
    }
}
