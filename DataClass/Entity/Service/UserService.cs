﻿using DataClass.Entity.Implement;
using DataClass.Entity.Interface;
using HotelSystem.Models;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Service
{
    public class UserService : Repository<User>, IUserService
    {
        public UserService(ApplicationDbContext repositoryContext)
            : base(repositoryContext)
        {
        }

    }
}
