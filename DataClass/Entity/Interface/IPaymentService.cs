﻿using DataClass.Entity.Implement;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Interface
{
    public interface IPaymentService : IRepository<Payment>
    {
        void AddPay(AddPayViewModel model);

        DayReportViewModel DayReport();
    }
}
