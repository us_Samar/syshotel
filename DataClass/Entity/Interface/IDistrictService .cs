﻿using DataClass.Entity.Implement;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Interface
{
    public interface IDistrictService : IRepository<District>
    {

    }
}
