﻿using DataClass.Entity.Implement;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using HotelSystem.Models.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Interface
{
    public interface IOrderService : IRepository<Order>
    {
        IList<OrdersViewModel> GetOrdersViewList();

        void AddOrders(AddNewOrderViewModel order);
        void UpdateOrders(EditOrderViewModel order);
    }
}
