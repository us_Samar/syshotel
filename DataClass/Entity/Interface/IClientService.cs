﻿using DataClass.Entity.Implement;
using DataClass.Models;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Interface
{
    public interface IClientService : IRepository<Client>
    {
        IEnumerable<ClientViewModel> GetClient();
        int SearchClient(ClientFilterModel model);
        string FIO(int id);
        string GenderName(int ClientID);
    }


}
