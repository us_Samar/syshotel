﻿using Abp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using SysHotel.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Implement
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected ApplicationDbContext ApplicationDbContext { get; set; }
        public Repository(ApplicationDbContext ApplicationDbContext)
        {
            this.ApplicationDbContext = ApplicationDbContext;
        }
        public IQueryable<T> FindAll()
        {
            return this.ApplicationDbContext.Set<T>().AsNoTracking();
        }

        public T Get(int Id)
        {
            return this.ApplicationDbContext.Set<T>().Find(Id);
        }
        public void Add(T entity)
        {
            this.ApplicationDbContext.Set<T>().Add(entity);
            this.ApplicationDbContext.SaveChanges();

        }
        public void Update(T entity)
        {
            this.ApplicationDbContext.Set<T>().Update(entity);
            this.ApplicationDbContext.SaveChanges();
        }
        public void Delete(T entity)
        {
            this.ApplicationDbContext.Set<T>().Remove(entity);
            this.ApplicationDbContext.SaveChanges();
        }


        //public virtual List<T> GetAll()
        //{
        //    return entities.ToList();
        //}

        //public virtual void Add(T entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException("entity");
        //    }
        //    entities.Add(entity);
        //    context.SaveChanges();
        //}

        //public virtual void Update(T entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException("entity");
        //    }
        //    context.SaveChanges();
        //}

        //public virtual void Remove(T entity)
        //{
        //    if (entity == null)
        //    {
        //        throw new ArgumentNullException("entity");
        //    }
        //    entities.Remove(entity);
        //    context.SaveChanges();
        //}
    }
}
