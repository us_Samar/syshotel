﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Entity.Implement
{
    public interface IEntity
    {
        IKey Id { get; set; }
    }
}
