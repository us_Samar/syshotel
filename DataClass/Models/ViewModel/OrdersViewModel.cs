﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class OrdersViewModel
    {
        public int Id { get; set; }
        public string AllName { get; set; }
        public string Payment { get; set; }
        public int AllPayment { get; set; }
        public string PaymentType { get; set; }
        public string RoomName { get; set; }
        public string Comment { get; set; }
        public int Gender { get; set; }
        public int? Status { get; set; }
        public int RoomId { get; set; }
        public int PaymentStatus { get; set; }
        public bool Brom { get; set; }
    }
}
