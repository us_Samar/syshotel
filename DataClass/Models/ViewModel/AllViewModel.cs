﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public  class AllViewModel
    {
        public int Id { get; set; }
        public string RoomName{ get; set; }
        public int RoomCount{ get; set; }
        public int ActiveRoomCount { get; set; }
        public string Comment { get; set; }
    }
}
