﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class PaymentViewModel
    {
        public int Id { get; set; }
        public string Payment { get; set; }
        public string PaymentTypeName { get; set; }
        public DateTime PaymentTime { get; set; }
    }
}
