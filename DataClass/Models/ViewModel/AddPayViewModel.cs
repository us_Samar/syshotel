﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class AddPayViewModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ClientId { get; set; }
        public int UsertId { get; set; }
        public int Payment { get; set; }
        public int Payment_type { get; set; }
        public string FIO { get; set; }

    }
}
