﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class EditOrderViewModel
    {
        public int Id { get; set; }
        public int ClientID { get; set;}
        public string FIO { get; set;}
        public int RoomID { get; set;}
        public int PaymentForDay { get; set;}
        public string Comment{ get; set;}
        public bool AllBusy { get; set;}
    }
}
