﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class ConnectOrderToRoom
    {
        public IEnumerable<OrdersViewModel> OrdersViewModels { get; set; }
        public IEnumerable<AllViewModel> AllViewModels { get; set; }
    }
}
