﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class ExpensesViewModel
    {
        public int Id { get; set; }
        public int Expense { get; set; }
        public string Comment { get; set; }
        public DateTime? Expense_date { get; set; }
        public string ExpensesTypeName { get; set; }
    }
}
