﻿using HotelSystem.Models;
using HotelSystem.Models.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class ClientViewModel
    {
        public int Id{ get; set; }
        public string FirstName { get; set; }=string.Empty;
        public string LastName { get; set; }=string.Empty;
        public string MiddleName { get; set; }=string.Empty;
        public DateTime Date_of_Birth { get; set; }
        public string Passport_Series { get; set; } = string.Empty;
        public string Passport_Number { get; set; }= string.Empty;
        public int Payment { get; set; }
        public int Payment_type { get; set; }
        public int PaymentForDay { get; set; }
        public string Comment { get; set; }=string.Empty;
        public int RoomName { get; set; }
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public bool AllBusy { get; set; }
        public string GenderName { get; set; }=string.Empty;
        

    }
         
}
