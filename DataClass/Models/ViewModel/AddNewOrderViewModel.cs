﻿using HotelSystem.Models;
using HotelSystem.Models.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class AddNewOrderViewModel
    {
        public int Id{ get; set; }
        public string FIO { get; set; }
        public int Payment { get; set; }
        public int Payment_type { get; set; }
        public int PaymentForDay { get; set; }
        public string Comment { get; set; }
        public int RoomName { get; set; }

        public int ClientID { get; set; }
        public int UserID { get; set; }
        public bool AllBusy { get; set; }

    }
    //public class PageList
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}
