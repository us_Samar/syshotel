﻿using HotelSystem.Models;
using HotelSystem.Models.Reference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.ViewModel
{
    public class DayReportViewModel
    {        
        public IEnumerable<ListClass> PayList { get; set; }

        public IEnumerable<ListClass> ExpenList { get; set; }
    }
    public class ListClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

}
