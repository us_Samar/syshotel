﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class Accessory_type
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
