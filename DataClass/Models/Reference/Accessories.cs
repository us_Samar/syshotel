﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Reference
{
    public class Accessories
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Accessory_name { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public int Count { get; set; }
        public DateTime? Time_of_delivery { get; set; }
        public virtual Room? Room { get; set; }
        public virtual Accessory_type Accessory_Type { get; set; }
    }
}
