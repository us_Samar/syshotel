﻿using DataClass.Models.NewModels;
using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Reference
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Comment{ get; set; }
        
        [Required]
        public DateTime TimeOfDelivery { get; set; }
        [Required]
        public DateTime? CheckOutTime { get; set; }
        [Required]
        public int DayPayment { get; set; }
        [Required]
        public  PaymentClient PaymentClient { get; set; }
        [Required]
        public  OrderStatus_type? OrderStatus_type { get; set; }
        [Required]
        public  Room Room { get; set; }
        [Required]
        public User User { get; set; }

        public DateTime DayOfDelivery { get; set; }
        public int PaymentClientId { get; set; }
        public int RoomId { get; set; }
        public int? OrderStatus_typeId { get; set; }
        public int UserId { get; set; }
        public bool Brom { get; set; }

    }
}
