﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Reference
{
    public class Room
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Floor { get; set; }
        [Required]
        public  RoomCapasity RoomCapasity { get; set; }
        public  int RoomCapasityId { get; set; }
        
    }
}
