﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class OrderStatus_type
    { 
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
