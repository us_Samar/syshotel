﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class Payment_type
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
