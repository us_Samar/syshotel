﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Reference
{
    public class RoomCapasity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Count { get; set; }
    }
}
