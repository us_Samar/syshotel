﻿using HotelSystem.Models;
using HotelSystem.Models.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClass.Models.NewModels
{
    public class PaymentClient
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int AllPayment{ get; set; }
        [Required]
        public  Payment Payment{ get; set; }
        [Required]
        public  Client Client{ get; set; }

        public int PaymentId { get; set; }
        public int ClientId { get; set; }
    }
}
