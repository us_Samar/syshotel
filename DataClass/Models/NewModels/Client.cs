﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Main
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        [Required]
        public string Passport_series { get; set; }
        [Required]
        public string Passport_number { get; set; }
        [Required]
        public DateTime? Date_of_birth { get; set; }
        public  District District { get; set; }
        public  Gender Gender { get; set; }

        public int DistrictId { get; set; }
        public int GenderId { get; set; }
    }
}
