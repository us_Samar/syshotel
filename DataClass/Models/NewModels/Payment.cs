﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class Payment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int PaymentValue { get; set; }
        [Required]
        public DateTime PaymentTime{ get; set; }
        [Required]
        public  Payment_type Payment_Type { get; set; }
        [Required]
        public int Payment_typeId { get; set;}
        public int UserId { get; set;}


    }
}
