﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models.Main
{
    public class Expenses_Type
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
