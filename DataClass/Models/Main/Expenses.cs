﻿namespace HotelSystem.Models.Main
{
    public class Expenses
    {
        public int Id { get; set; }
        public int Expense { get; set; }
        public string Comment { get; set; }
        public DateTime Expens_date { get; set; }
        public virtual Expenses_Type Expenses_Type { get; set; }
        public virtual Payment_type Payment_type { get; set; }
        public virtual User User { get; set; }
        public int Expenses_TypeId { get; set; }
        public int Payment_typeId { get; set; }
        public int UserId { get; set; }
    }
}
