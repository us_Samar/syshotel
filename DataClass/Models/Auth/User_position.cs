﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class User_position
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
