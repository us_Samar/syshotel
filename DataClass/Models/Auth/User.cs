﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Middlename { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public DateTime StartWork { get; set; }
        public DateTime? EndWork { get; set; }
        public virtual User_role UserRole { get; set; }
        public virtual User_position Userposition { get; set; }
        public string Gender { get; set; }
        public int UserRoleId { get; set; }
        public int UserpositionId { get; set; }

    }
}
