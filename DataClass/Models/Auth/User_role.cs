﻿using System.ComponentModel.DataAnnotations;

namespace HotelSystem.Models
{
    public class User_role
    {
        [Key]
        public int Id { get; set; }        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}
