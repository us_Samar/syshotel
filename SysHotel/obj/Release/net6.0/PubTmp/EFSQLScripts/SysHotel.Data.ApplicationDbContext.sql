﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Districts] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Districts] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Expenses_Types] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Expenses_Types] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Genders] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Genders] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [OrderStatus_type] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_OrderStatus_type] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Payment_types] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Payment_types] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Regions] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Regions] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [RoomCapasitys] (
        [Id] int NOT NULL IDENTITY,
        [Count] int NOT NULL,
        CONSTRAINT [PK_RoomCapasitys] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [User_positions] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_User_positions] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [User_roles] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(100) NOT NULL,
        CONSTRAINT [PK_User_roles] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Clients] (
        [Id] int NOT NULL IDENTITY,
        [FirstName] nvarchar(max) NOT NULL,
        [LastName] nvarchar(max) NOT NULL,
        [MiddleName] nvarchar(max) NOT NULL,
        [Passport_series] nvarchar(max) NOT NULL,
        [Passport_number] nvarchar(max) NOT NULL,
        [Date_of_birth] datetime2 NOT NULL,
        [DistrictId] int NOT NULL,
        [GenderId] int NOT NULL,
        CONSTRAINT [PK_Clients] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Clients_Districts_DistrictId] FOREIGN KEY ([DistrictId]) REFERENCES [Districts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Clients_Genders_GenderId] FOREIGN KEY ([GenderId]) REFERENCES [Genders] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Payments] (
        [Id] int NOT NULL IDENTITY,
        [PaymentValue] int NOT NULL,
        [PaymentTime] datetime2 NOT NULL,
        [Payment_typeId] int NOT NULL,
        [UserId] int NOT NULL,
        CONSTRAINT [PK_Payments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Payments_Payment_types_Payment_typeId] FOREIGN KEY ([Payment_typeId]) REFERENCES [Payment_types] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Rooms] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NOT NULL,
        [Floor] int NOT NULL,
        [RoomCapasityId] int NOT NULL,
        CONSTRAINT [PK_Rooms] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Rooms_RoomCapasitys_RoomCapasityId] FOREIGN KEY ([RoomCapasityId]) REFERENCES [RoomCapasitys] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Users] (
        [Id] int NOT NULL IDENTITY,
        [FirstName] nvarchar(max) NOT NULL,
        [LastName] nvarchar(max) NOT NULL,
        [Middlename] nvarchar(max) NOT NULL,
        [Login] nvarchar(max) NOT NULL,
        [Password] nvarchar(max) NOT NULL,
        [StartWork] datetime2 NOT NULL,
        [EndWork] datetime2 NULL,
        [Gender] nvarchar(max) NOT NULL,
        [UserRoleId] int NOT NULL,
        [UserpositionId] int NOT NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Users_User_positions_UserpositionId] FOREIGN KEY ([UserpositionId]) REFERENCES [User_positions] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Users_User_roles_UserRoleId] FOREIGN KEY ([UserRoleId]) REFERENCES [User_roles] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [PaymentClients] (
        [ID] int NOT NULL IDENTITY,
        [AllPayment] int NOT NULL,
        [PaymentId] int NOT NULL,
        [ClientId] int NOT NULL,
        CONSTRAINT [PK_PaymentClients] PRIMARY KEY ([ID]),
        CONSTRAINT [FK_PaymentClients_Clients_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [Clients] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_PaymentClients_Payments_PaymentId] FOREIGN KEY ([PaymentId]) REFERENCES [Payments] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Expenseses] (
        [Id] int NOT NULL IDENTITY,
        [Expense] int NOT NULL,
        [Comment] nvarchar(max) NOT NULL,
        [Expens_date] datetime2 NOT NULL,
        [Expenses_TypeId] int NOT NULL,
        [Payment_typeId] int NOT NULL,
        [UserId] int NOT NULL,
        CONSTRAINT [PK_Expenseses] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Expenseses_Expenses_Types_Expenses_TypeId] FOREIGN KEY ([Expenses_TypeId]) REFERENCES [Expenses_Types] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Expenseses_Payment_types_Payment_typeId] FOREIGN KEY ([Payment_typeId]) REFERENCES [Payment_types] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Expenseses_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE TABLE [Orders] (
        [Id] int NOT NULL IDENTITY,
        [Comment] nvarchar(max) NOT NULL,
        [TimeOfDelivery] datetime2 NOT NULL,
        [CheckOutTime] datetime2 NOT NULL,
        [DayPayment] int NOT NULL,
        [DayOfDelivery] datetime2 NOT NULL,
        [PaymentClientId] int NOT NULL,
        [RoomId] int NOT NULL,
        [OrderStatus_typeId] int NOT NULL,
        [UserId] int NOT NULL,
        [Brom] bit NOT NULL,
        CONSTRAINT [PK_Orders] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Orders_OrderStatus_type_OrderStatus_typeId] FOREIGN KEY ([OrderStatus_typeId]) REFERENCES [OrderStatus_type] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Orders_PaymentClients_PaymentClientId] FOREIGN KEY ([PaymentClientId]) REFERENCES [PaymentClients] ([ID]) ON DELETE CASCADE,
        CONSTRAINT [FK_Orders_Rooms_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [Rooms] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Orders_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Clients_DistrictId] ON [Clients] ([DistrictId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Clients_GenderId] ON [Clients] ([GenderId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Expenseses_Expenses_TypeId] ON [Expenseses] ([Expenses_TypeId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Expenseses_Payment_typeId] ON [Expenseses] ([Payment_typeId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Expenseses_UserId] ON [Expenseses] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Orders_OrderStatus_typeId] ON [Orders] ([OrderStatus_typeId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Orders_PaymentClientId] ON [Orders] ([PaymentClientId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Orders_RoomId] ON [Orders] ([RoomId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Orders_UserId] ON [Orders] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_PaymentClients_ClientId] ON [PaymentClients] ([ClientId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_PaymentClients_PaymentId] ON [PaymentClients] ([PaymentId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Payments_Payment_typeId] ON [Payments] ([Payment_typeId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Rooms_RoomCapasityId] ON [Rooms] ([RoomCapasityId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Users_UserpositionId] ON [Users] ([UserpositionId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    CREATE INDEX [IX_Users_UserRoleId] ON [Users] ([UserRoleId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220727144911_AllMigration')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220727144911_AllMigration', N'6.0.0');
END;
GO

COMMIT;
GO

