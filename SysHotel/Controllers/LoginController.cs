﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SysHotel.Data;
using System.Security.Claims;

namespace SysHotel.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

        public LoginController(UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ApplicationDbContext db
            )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            _db = db;
        }
        const string UserId = "_id";
        const string UserName  = "_name";
        public IActionResult Login(int ErrorId=0)
        {
            if (ErrorId==1)
            {
                ViewBag.ErrorMassage = "login or password is incorrect";
            }
            return View();
        }
        [HttpPost]
        public IActionResult Login(string userName,string password)
        {
            if (string.IsNullOrEmpty(userName)&&string.IsNullOrEmpty(password))
            {
                return RedirectToAction("Login");
            }
            var user = _db.Users.FirstOrDefault(m=>m.Login== userName && m.Password== password);
            if (user!=null)
            {
                //create the identity for the user
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Login),
                    new Claim(ClaimTypes.Role, user.UserRoleId.ToString()),
                },CookieAuthenticationDefaults.AuthenticationScheme);

                var principal=new ClaimsPrincipal(identity);

                var result=HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, 
                    principal);

                signInManager.PasswordSignInAsync(userName, password, false,false);
                HttpContext.Session.SetInt32(UserId, user.Id);
                HttpContext.Session.SetString(UserName, user.Login);
                return RedirectToAction("Index","Home");

            }
            return RedirectToAction("Login",new {ErrorId=1});
        }


        [HttpGet]
        //[ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            signInManager.SignOutAsync();
            return RedirectToAction(nameof(LoginController.Login), "Login");
        }
    }
}
