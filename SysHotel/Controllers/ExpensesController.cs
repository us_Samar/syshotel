﻿using DataClass.Entity.Interface;
using DataClass.Models;
using DataClass.Models.ViewModel;
using HotelSystem.Models.Main;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SysHotel.Data;

namespace SysHotel.Controllers
{
    [Authorize]
    public class ExpensesController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IClientService _clientService;
        private readonly IUserService _userService;
        public int UserID;
        public ExpensesController(ApplicationDbContext db,
             IUserService userService,
             IClientService clientService)
        {
            _db = db;
            _userService = userService;
            _clientService = clientService;
        }
        const string UserId = "_id";
        const string UserName = "_name";
        public IActionResult Index()
        {
            List<ExpensesViewModel> expensesViewModel = new List<ExpensesViewModel>();
            var ExpensesList=_db.Expenseses.Where(m=>m.Expens_date.Year== DateTime.Now.Year&& 
                                                  m.Expens_date.Month == DateTime.Now.Month&&
                                                  m.Expens_date.Day == DateTime.Now.Day).ToList();
            foreach (var item in ExpensesList)
            {
                ExpensesViewModel list=new ExpensesViewModel();
                list.Id = item.Id;
                list.Expense = item.Expense;
                list.Expense_date=item.Expens_date;
                list.Comment = item.Comment;    
                list.ExpensesTypeName=_db.Expenses_Types.Find(item.Expenses_TypeId).Name;
                expensesViewModel.Add(list);
            }
            return View(expensesViewModel);
        }
        public IActionResult CreateExpenses(int Id=0)
        {
            Expenses model = new Expenses();
            var ExpensesTypeList = _db.Expenses_Types.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.ExpensesTypeList=ExpensesTypeList;
            var PaymentTypeList = _db.Payment_types.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.PaymentTypeList = PaymentTypeList;
            if (Id>0)
            {
                var update=_db.Expenseses.Find(Id);
                return View(update);
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateExpenses(Expenses model)
        {
            if (model.Id > 0)
            {
                model.UserId = (int)HttpContext.Session.GetInt32(UserId); 
                _db.Expenseses.Update(model);
                _db.SaveChanges();
            }
            else
            {
                model.Expens_date = DateTime.Now;
                model.UserId = (int)HttpContext.Session.GetInt32(UserId); 
                _db.Expenseses.Add(model);
                _db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public IActionResult EditExpenses(int Id)
        {
            var ExpensesTypeList = _db.Expenses_Types.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return View();
        }
        public IActionResult SearchClient(ClientFilterModel model)
        {
            int ClId = _clientService.SearchClient(model);
            return RedirectToAction("Index", new { ClientId = ClId }); 
        }
    }
}
