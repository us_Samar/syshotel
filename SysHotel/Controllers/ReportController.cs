﻿using DataClass.Entity.Interface;
using DataClass.Models;
using DataClass.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SysHotel.Data;



namespace SysHotel.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IPaymentService _paymentService;

        public ReportController(ApplicationDbContext db,
            IPaymentService paymentService
            )
        {
            _db = db;
            _paymentService = paymentService;
        }
        public IActionResult Index()
        {
            var returnModel=_paymentService.DayReport();
            return View(returnModel);
        }
    }
}
