﻿using DataClass.Entity.Interface;
using DataClass.Models.ViewModel;
using HotelSystem.Models.Main;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SysHotel.Data;

namespace SysHotel.Controllers
{
    public class ClientController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IClientService _clientService;
        public ClientController(ApplicationDbContext db,
        IClientService clientService)
        {
            _db = db;// bu asosiy Connect hamma Controllerda bo'ladi
            _clientService = clientService;
        }

        public IActionResult Index()
        {            
            var list =_clientService.GetClient();
            return View(list);
        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        {         
            var ClientList =_db.Clients.ToList().FirstOrDefault(m => m.Id ==Id);
            if (ClientList == null)
            {
                return NotFound();
            }
            return View(ClientList);
        }
      
        public IActionResult Edit(Client model)
        {
            var entity = _db.Clients.FirstOrDefault(m => m.Id == model.Id);
            Client client = new Client();
            if (entity == null)
                return View("No content");
            entity.FirstName = client.FirstName;
            entity.LastName = client.LastName;
            entity.MiddleName = client.MiddleName;
            entity.Date_of_birth = client.Date_of_birth;
            entity.Passport_number = client.Passport_number;
            entity.Passport_series = client.Passport_series;
            _db.Clients.Update(entity);
            _db.SaveChanges();
            return View(entity);           

        }

        [HttpPost]
        public IActionResult Index(string ClientSearch)
        {
            ViewData["GetClientDetails"] = ClientSearch;
            var Clientquery = from x in _db.Clients select x;
            if (!String.IsNullOrEmpty(ClientSearch))
            {
                Clientquery = Clientquery.Where(x => x.FirstName.Contains(ClientSearch) || x.LastName.Contains(ClientSearch)||x.MiddleName.Contains(ClientSearch));
            }
            return View(Clientquery.ToList());
        }
    }
}
