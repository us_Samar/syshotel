﻿using DataClass.Entity.Interface;
using DataClass.Models;
using DataClass.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SysHotel.Data;

namespace SysHotel.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IClientService _clientService;

        public PaymentController(ApplicationDbContext db,
        IClientService clientService)
        {
            _db= db;
            _clientService= clientService;
        }
        public IActionResult Index(int ClientId=0)
        {
            List<PaymentViewModel> model=new List<PaymentViewModel>();
            if (ClientId!=0)
            {
                var paymentOrder = _db.PaymentClients.Where(m => m.ClientId == ClientId).ToList();
                foreach (var item in paymentOrder)
                {
                    var payment = _db.Payments.Find(item.PaymentId);
                    if (payment != null)
                    {
                        PaymentViewModel entity = new PaymentViewModel();
                        entity.Id = payment.Id;
                        entity.Payment = payment.PaymentValue.ToString();
                        entity.PaymentTypeName = _db.Payment_types.Find(payment.Payment_typeId).Name;
                        entity.PaymentTime = payment.PaymentTime;
                        model.Add(entity);
                    }
                }
            }
            else
            {
                var PaymentList = _db.Payments.Where(m => m.PaymentTime.Day == DateTime.Now.Day);
                foreach (var item in PaymentList)
                {
                    PaymentViewModel entity = new PaymentViewModel();
                    entity.Id = item.Id;
                    entity.Payment = item.PaymentValue.ToString();
                    entity.PaymentTypeName = _db.Payment_types.Find(item.Payment_typeId).Name;
                    entity.PaymentTime=item.PaymentTime;
                    model.Add(entity);
                }
                ViewBag.Error = "No client found";
            }
            return View(model);
        }
        public IActionResult SearchClient(ClientFilterModel model)
           {
            int ClId = _clientService.SearchClient(model);
            return RedirectToAction("Index", new { ClientId = ClId });
        }

       


    }
}
