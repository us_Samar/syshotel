﻿        using DataClass.Entity.Interface;
using DataClass.Models;
using DataClass.Models.NewModels;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SysHotel.Data;
using System.Security.Claims;

namespace SysHotel.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IUserService _userService;
        private readonly IClientService _clientService;
        private readonly IOrderService _orderService;
        private readonly IDistrictService _districtService;
        private readonly IPaymentService _paymentService;
        public OrderController(IClientService clientService,
             IUserService userService,
            IOrderService orderService,
            IDistrictService districtService,
            IPaymentService paymentService,
            ApplicationDbContext db)
        {
            _db = db;
            _userService = userService;
            _clientService = clientService;
            _orderService = orderService;
            _districtService = districtService;
            _paymentService = paymentService;
        }
        const string UserId = "_id";
        const string UserName = "_name";
        public IActionResult Index()
        {
            ConnectOrderToRoom returnModel=new ConnectOrderToRoom();
            var model = _orderService.GetOrdersViewList().OrderBy(x=>x.RoomId);
            List<AllViewModel> allView = new List<AllViewModel>();

            var roomList = _db.Rooms.ToList();
            foreach (var item in roomList)
            {
                var count = model.Where(m => m.Status == 1 && m.RoomId == item.Id).ToList();
                AllViewModel entity = new AllViewModel();
                entity.Id = item.Id;
                entity.RoomName = item.Name + " (" + item.Floor + "-floor)";
                if (count.Where(m => m.Brom == true).Count() > 0)
                {
                    entity.ActiveRoomCount = _db.RoomCapasitys.Find(item.RoomCapasityId).Count;
                }
                else
                {
                    entity.ActiveRoomCount = model.Where(m => m.Status == 1 && m.RoomId == item.Id).Count();
                }
                entity.RoomCount = _db.RoomCapasitys.Find(item.RoomCapasityId).Count - entity.ActiveRoomCount;
                var comment = model.Where(m => m.Status == 1 && m.RoomId == item.Id && m.Comment != null).FirstOrDefault();
                entity.Comment = comment == null ? "" : comment.Comment;
                allView.Add(entity);
            }
            returnModel.OrdersViewModels = model;
            returnModel.AllViewModels = allView;
            return View(returnModel);
        }
        public IActionResult EditOrder(int OrderId)
        {
            var order = _db.Orders.Find(OrderId);
            if (order != null)
            {
                var payclient = _db.PaymentClients.Find(order.PaymentClientId);

                EditOrderViewModel model = new EditOrderViewModel();
                model.Id = order.Id;
                var RoomList = _db.Rooms.Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                ViewBag.RoomList = RoomList;
                var Client = _db.Clients.Find(payclient?.ClientId);
                model.FIO = Client?.FirstName + " " + Client?.LastName + " " + Client?.MiddleName;
                model.ClientID = Client.Id;
                model.PaymentForDay = order.DayPayment;
                model.AllBusy = order.Brom;
                model.Comment = order.Comment;
                model.RoomID = order.RoomId;
                return View(model);
            }
            return View();
        }
        [HttpPost]
        public IActionResult EditOrder(EditOrderViewModel model)
        {
            _orderService.UpdateOrders(model);
            return RedirectToAction("Index");
        }
        public IActionResult AddPay(int OrderId)
        {
            AddPayViewModel model=new AddPayViewModel();
            var order = _db.Orders.Find(OrderId);
            var payclient = _db.PaymentClients.Find(order.PaymentClientId);
            var PaymentTypeList = _db.Payment_types.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.PaymentTypeList = PaymentTypeList;
            var Client = _db.Clients.Find(payclient?.ClientId);
            model.FIO = Client?.FirstName + " " + Client?.LastName + " " + Client?.MiddleName;
            model.ClientId = Client.Id;
            return View(model);
        }
        [HttpPost]
        public IActionResult AddPay(AddPayViewModel model)
        {
            if (model==null)
            {
                return null;
            }
            model.UsertId = (int)HttpContext.Session.GetInt32(UserId);
            _paymentService.AddPay(model);
            return RedirectToAction("Index");
        }
        public IActionResult CreateClient(int error=0)
        {
            var Genderlist = _db.Genders.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.Genderlist = Genderlist;
            if (error>0)
            {
                ViewBag.Message = "This person already exists";
            }
            return View();
        }
        [HttpPost]
        public IActionResult CreateClient(Client model)
        {
            if (model!=null)
            {
                model.DistrictId = 1;
                var FindClient=_db.Clients.Where(m=>m.Passport_series==model.Passport_series&&
                                                    m.Passport_number == model.Passport_number).FirstOrDefault();
                if (FindClient == null)
                {
                    _clientService.Add(model);
                    return RedirectToAction("CreateOrder", new { ClientId = model.Id });
                }
            }
            return RedirectToAction("CreateClient", new {error=1});
        }
        public IActionResult CreateOrder(int OrderId=0,int ClientId=0)
        {
            AddNewOrderViewModel model = new AddNewOrderViewModel();
            var PaymentTypeList = _db.Payment_types.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            var RoomList = _db.Rooms.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.RoomList = RoomList;
            ViewBag.PaymentTypeList = PaymentTypeList;
            model.FIO = "No client found";
            model.ClientID = 0;
            if (ClientId>0)
            {
                var Client = _db.Clients.Find(ClientId);
                model.FIO = Client.FirstName + " " + Client.LastName + " " + Client.MiddleName;
                model.ClientID = Client.Id;
            }

            if (OrderId >0)
            {
                var Order =_orderService.Get(OrderId);
                if (Order != null)
                {
                    var payClient=_db.PaymentClients.Find(Order.Id);
                    var pay=_db.Payments.Find(payClient.PaymentId);
                    var Client = _db.Clients.Find(payClient.ClientId);
                    model.Id = Order.Id;
                    model.ClientID = Client.Id;
                    model.FIO= Client.FirstName + " " + Client.LastName + " " + Client.MiddleName;
                    model.Payment_type = pay.Payment_typeId;
                    model.RoomName = Order.RoomId;
                    model.Payment = pay.PaymentValue;
                    model.PaymentForDay = Order.DayPayment;
                    model.Comment = Order.Comment;
                    model.AllBusy = Order.Brom;
                }
            }

            return View(model);
        }
        [HttpPost]
        public IActionResult CreateOrder(AddNewOrderViewModel model)
        {
            try
            {
                model.UserID = (int)HttpContext.Session.GetInt32(UserId);
                _orderService.AddOrders(model);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IActionResult CheckOut(int OrderID)
        {
            var order=_orderService.Get(OrderID);
            order.OrderStatus_typeId = 2;
            order.CheckOutTime = DateTime.Now;
            _orderService.Update(order);
            return RedirectToAction("Index");
        }

        public IActionResult SearchClient(ClientFilterModel model)
        {
            int ClId=_clientService.SearchClient(model);
            return RedirectToAction("CreateOrder",new { ClientId = ClId});
        }
    }
}
