﻿   using DataClass.Entity.Interface;
using DataClass.Models.ViewModel;
using HotelSystem.Models;
using HotelSystem.Models.Main;
using Microsoft.AspNetCore.Mvc;
using SysHotel.Data;

namespace SysHotel.Controllers
{
    public class ReferenceController : Controller
    {
        private readonly ApplicationDbContext _db;
      
        private readonly IDistrictService _districtService;
        public ReferenceController(IDistrictService districtService,
            ApplicationDbContext db)
        {

            _db = db;            
            _districtService = districtService;
        }
        public IActionResult IndexRegion()
        {
            var lists=_db.Regions.ToList();
            return View(lists);           
        }

        public IActionResult IndexDistrict()
        {            
            var listDistrict = _db.Districts.ToList();
            return View(listDistrict);
        }

        public IActionResult IndexExpensesType()
        {
            var listExpensesType = _db.Expenses_Types.ToList();
            return View(listExpensesType);
        }
        #region Region 


        [HttpGet]
        public IActionResult AddOrUpdateRegion(int Id=0)
        {
            Region model=new Region();
            if (Id!=0)
            {
                var searchregion = _db.Regions.Find(Id);
                model.Id= searchregion.Id;
                model.Name = searchregion.Name;
            }
            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult AddOrUpdateRegion( Region model)
        {
            try
            {
                if (model.Id == 0)
                {
                    var entity = new Region
                    {
                        Name = model.Name
                    };
                    _db.Regions.Add(entity); 
                    _db.SaveChanges();
                }
                else
                {
                    var entity = _db.Regions.Find(model.Id);
                    if (entity == null)
                        return View("No content");
                    entity.Name = model.Name;
                    _db.Regions.Update(entity);
                    _db.SaveChanges();

                }
                return RedirectToAction("IndexRegion");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult DeleteRegion(int id)
        {
            try
            {
                var regionId=_db.Regions.Find(id);
                _db.Regions.Remove(regionId);
                _db.SaveChanges();
                return RedirectToAction("IndexRegion");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }
        #endregion

        #region Dictrict 


        [HttpGet]
        public IActionResult AddOrUpdateDistrict(int Id = 0)
        {
            District model = new District();
            if (Id != 0)
            {
                var searchDistrict = _db.Districts.Find(Id);
                model.Id = searchDistrict.Id;
                model.Name = searchDistrict.Name;
            }
            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult AddOrUpdateDistrict(District model)
        {
            try
            {
                if (model.Id == 0)
                {
                    var entity = new District
                    {
                        Name = model.Name
                    };
                    _db.Districts.Add(entity);
                    _db.SaveChanges();
                }
                else
                {
                    var entity = _db.Districts.Find(model.Id);
                    if (entity == null)
                        return View("No content");
                    entity.Name = model.Name;
                    _db.Districts.Update(entity);
                    _db.SaveChanges();

                }
                return RedirectToAction("IndexDistrict");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult DeleteDistrict(int id)
        {
            try
            {
                var DistrictId = _db.Districts.Find(id);
                _db.Districts.Remove(DistrictId);
                _db.SaveChanges();
                return RedirectToAction("IndexDistrict");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }
        #endregion

        #region ExpensesType 


        [HttpGet]
        public IActionResult AddOrUpdateExpensesType(int Id = 0)
        {
            Expenses_Type model = new Expenses_Type();
            if (Id != 0)
            {
                var searchDistrict = _db.Expenses_Types.Find(Id);
                model.Id = searchDistrict.Id;
                model.Name = searchDistrict.Name;
            }
            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult AddOrUpdateExpensesType(Expenses_Type model)
        {
            try
            {
                if (model.Id == 0)
                {
                    var entity = new Expenses_Type
                    {
                        Name = model.Name
                    };
                    _db.Expenses_Types.Add(entity);
                    _db.SaveChanges();
                }
                else
                {
                    var entity = _db.Expenses_Types.Find(model.Id);
                    if (entity == null)
                        return View("No content");
                    entity.Name = model.Name;
                    _db.Expenses_Types.Update(entity);
                    _db.SaveChanges();

                }
                return RedirectToAction("IndexExpensesType");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[CustomAuthorize(Permission.ReferenceEdit)]
        public IActionResult DeleteExpensesType(int id)
        {
            try
            {
                var DistrictId = _db.Expenses_Types.Find(id);
                _db.Expenses_Types.Remove(DistrictId);
                _db.SaveChanges();
                return RedirectToAction("IndexExpensesType");
            }
            catch (Exception ex)
            {
                return View(ex);
            }
        }

        #endregion







    }
}
